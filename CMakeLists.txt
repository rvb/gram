cmake_minimum_required(VERSION 2.8)
project(hindex)
include_directories(${CMAKE_CURRENT_BINARY_DIR})

set (gram_VERSION_MAJOR 0)
set (gram_VERSION_MINOR 2)
set (gram_NAME \"gram\")

find_package(Boost 1.36 COMPONENTS graph REQUIRED)

set(CMAKE_CXX_FLAGS "-g3 -O3")

INCLUDE_DIRECTORIES( ${Boost_INCLUDE_DIR} )

add_executable(gram src/gram.cpp src/ColorSets.cpp src/GraphMotifInstance.cpp src/GraphUtils.cpp src/LUT.cpp)

target_link_libraries(gram ${Boost_LIBRARIES})
