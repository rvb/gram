/*   Gram -- A graph motif solver.
 *
 *   (C) 2008-2016 René van Bevern <rvb@nsu.ru>
 *
 *   This file is part of Gram.
 *
 *   Gram is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Gram is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Gram.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <boost/bind.hpp>
#include "ColorSets.hpp"
#include "GraphUtils.hpp"
#include "GraphMotifInstance.hpp"
#include "SubsetIterator.hpp"

using namespace std;

void usage(const char *th) {
  cout << "Gram, (C) 2008-2016 Rene van Bevern <rvb@nsu.ru>" << endl
       << "-------------------------------------------------------------" << endl;
  cout << "Usage: " << th << " [options] graphviz_file.dot" << endl
       << "Options are: " << endl
       << "  -e <float>         : set error probability, lower" <<endl
       << "                       probability increases runtime" << endl
       << "  -p                 : use algorithm whose exponential" << endl
       << "                       running time part only depends on" << endl
       << "                       size of found submotif, instead" << endl
       << "                       of size of sought motif" << endl
       << "  -b                 : brute force motif instance guessing" << endl
       << "graphviz_file.dot is the graph to work on. If this is \"-\" the" << endl
       << "graphviz graph is read from standard input" << endl;
  exit(1);
}    

// void simpleStatus(int tries, int done) {
//   printf("Progress                  : %3.2f%%\r",
// 	 (double)done / (double)tries * 100);
//   fflush(stdout);
// }

// bool is_listcolored(const Graph& G) {
//   VertexIterator v0, vn;
//   tie(v0,vn) = vertices(G);
//   for(; v0 != vn; ++v0)
//     if(ColorSet(get(vertex_rank, G, *v0)).size() > 1) return true;
//   return false;
// }

int main (int argc, char *argv[]) {
  int c;
  bool p = false;
  bool b = false;
  float e = 0.1;
  opterr = 0;

  while ((c = getopt(argc, argv, "e:pb")) != -1)
    switch (c) {
    default: usage(argv[0]);
    case 'e': {
      e = atof(optarg);
      assert(e > 0);
      break;
    }
    case 'p': {
      p = true;
      break;
    }
    case 'b': {
      b = true;
      break;
    }
    }

  if(optind >= argc) {
    cout << "Error: no filename given" << endl;
    usage(argv[0]);
  }

  Graph G(readGraphvizFromFile(argv[optind]));
  MultiColorSet M(motifFromGraph(G));
//  cout << is_listcolored(G) << " " << M.size() << endl;
//  exit(EXIT_SUCCESS);

  cout << "colorful motif size       : " << M.size() << endl;
  remove_vertices(G,isolated);
  M = motifFromGraph(G);
  cout << "maximum error probability : " << e << endl;
  
  Solution result;
  VertexWeight weight;
  if(p) tie(weight, result) = solve_components_independently<MaxMotifInstance>(G, M, e);
  else if(b) tie(weight, result) = solve_components_independently<BruteForceInstance>(G, M, e);
  else {
//    GraphMotifInstance GMI(G,M,e);
    tie(weight, result) = solve_components_independently<GraphMotifInstance>(G, M, e);
  }

  cout << "solution size             : " << (result.size()==0?1:result.size()) << endl;
  cout << "solution weight           : " << weight << endl;
  cout << "solution vertices         : ";
  if(result.empty()) { cout << "No nontrivial motif occurrence found." << endl; }
  else {
    copy(result.begin(), result.end(), ostream_iterator<VertexID>(cout, " "));
    cout << endl;
  }
  exit(EXIT_SUCCESS);
}
