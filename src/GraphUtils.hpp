/*   Gram -- A graph motif solver.
 *
 *   (C) 2008-2016 René van Bevern <rvb@nsu.ru>
 *
 *   This file is part of Gram.
 *
 *   Gram is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Gram is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Gram.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GRAPH_UTILS_H
#define _GRAPH_UTILS_H
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/function.hpp>

#include "ColorSets.hpp"

using namespace boost;

typedef int VertexID;
typedef int VertexWeight;
typedef int EdgeWeight;
typedef std::size_t VertexIndex;
typedef uint64_t ColorList;

/// vertex property type: colors and names
typedef property<vertex_index_t, VertexIndex,
		 property<vertex_color_t, Color,
			  property<vertex_name_t, VertexID,
				   property<vertex_rank_t, ColorList> > > > VertexInfo;

typedef property<edge_weight_t, EdgeWeight> EdgeInfo;

/// graph type
typedef adjacency_list<listS, listS, undirectedS, VertexInfo, EdgeInfo> Graph;

/// map from vertices to their colors
typedef property_map<Graph, vertex_color_t>::type VertexColors;

/// map from vertices to their names
typedef property_map<Graph, vertex_name_t>::type VertexNames;

/// map from vertices to their color list
typedef property_map<Graph, vertex_rank_t>::type VertexColorLists;

/// map from edges to their weights
typedef property_map<Graph, edge_weight_t>::type EdgeWeights;

typedef property_map<Graph, vertex_index_t>::type VertexIndices;

/// iterator over all graph vertices
typedef graph_traits<Graph>::vertex_iterator VertexIterator;

/// iterator over all graph edges
typedef graph_traits<Graph>::edge_iterator EdgeIterator;

/// vertex type
typedef graph_traits<Graph>::vertex_descriptor Vertex;

/// edge type
typedef graph_traits<Graph>::edge_descriptor Edge;

/// predicate type for edge filtering
typedef function2<bool, const Graph&, Edge> EdgeFilter;

/// predicate type for vertex filtering
typedef function2<bool, const Graph&, Vertex> VertexFilter;

/** read ::Graph from input stream. The input stream has to be in
    GraphViz format (so that it can be visualized with dot or neato),
    but the vertex names need to be numbers and need to have an
    integer attribute [color = x].
    
    @param src stream to read ::Graph from
    @return a ::Graph representing the input from @a src
 */
Graph readGraphviz(istream& src);

/** read ::Graph from file with ::readGraphviz, using the file at @a
 * filename as input stream.
   @param filename File to read ::Graph from
   @return a ::Graph representing the input from @a filename
 */
Graph readGraphvizFromFile(const char* filename);

/** remove edges in graph according to a given predicate
    @param G the graph to work on

    @param f an ::EdgeFilter function that is given the graph @a G and
    an edge. The function should return true if the edge
    should be removed from the graph, false otherwise.
    
    @post @a G has no edges satisfying @a f. */
Graph& remove_edges(Graph &G, const EdgeFilter& f);

/** remove vertices in graph according to a given predicate
    @param G the graph to work on

    @param f a ::VertexFilter function that is given the graph @a G and
    the a vertex. The function should return true if the vertex
    should be removed from the graph, false otherwise.
    
    @post @a G has no edges satisfying @a f. */
Graph& remove_vertices(Graph &G, const VertexFilter& f);

/** generates a colorful motif from list-colored graphs only */
MultiColorSet motifFromGraph(Graph &G);

/** @a is_not_in(M) for a collection @a M returns a
    ::VertexFilter predicate matching all vertices not in
    @a M.  @tparam T type of container for vertices. */
template <class T>
class is_not_in {
private: const T &M;
public:
  is_not_in(const T &_M) : M(_M) {}
  bool operator() (const Graph & G, Vertex v) {
    return (!member(v, M));
  }
};

template <class T>
class index_not_in {
private: const T &M;
public:
  index_not_in(const T &_M) : M(_M) {}
  bool operator() (const Graph & G, Vertex v) {
    return (!member(get(vertex_index, G, v), M));
  }
};


template <class T>
class index_not_mapsto {
private:
  const typename T::mapped_type to;
  const T &in;
public:
  index_not_mapsto(const T &_in, const typename T::mapped_type _to) : in(_in), to(_to) {}
  bool operator() (const Graph &G, const Vertex &v) const {
    typename T::const_iterator p = in.find(get(vertex_index, G, v));
    return (p == in.end()) || (p->second != to);
  }
};

/** a ::VertexFilter predicate matching isolated vertices. Returns
 * true if @a v is an isolated vertex in @a G */

inline bool isolated(const Graph& G, Vertex v) {
  Graph::adjacency_iterator a0, an;
  tie(a0, an) = adjacent_vertices(v, G);
  return (a0 == an);
}

/** an ::EdgeFilter predicate matching edges between vertices of the
    same color. Returns true of @a is an edge between to vertices of
    the same color */
inline bool unicolored_edge(const Graph& G, Edge e) {
  return get(vertex_color, G, source(e, G)) == get(vertex_color, G, target(e, G));
}

class colorlist_disjoint_from {
private: const ColorSet &M;
public:
  colorlist_disjoint_from(const ColorSet &_M) : M(_M) {}
  bool operator() (const Graph &G, Vertex v) {
    return (ColorSet(get(vertex_rank, G, v)) & M).empty();
  }
};

vector<vector<Vertex> > colorset_intersection_graph(Graph &G);
#endif
