/*   Gram -- A graph motif solver.
 *
 *   (C) 2008-2009 René van Bevern <rvb@nsu.ru>
 *
 *   This file is part of Gram.
 *
 *   Gram is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Gram is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Gram.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LUT_H
#define _LUT_H

#include <boost/unordered_set.hpp>
#include <boost/variant.hpp>
#include <list>
#include "ColorSets.hpp"
#include "GraphUtils.hpp"

using namespace boost;

struct Leaf {};
struct Add { Vertex v, u; ColorSet vc, uc;
  Add(Vertex v_, ColorSet vc_,
      Vertex u_, ColorSet uc_):
    v(v_), vc(vc_), u(u_), uc(uc_) {}};

/** backtracking table. Maps pairs of vertices and color sets to a
 * backtracking event of type ::TrackInfo. */
typedef variant<Leaf, Add> TrackInfo;

class LUT {
private:
  unordered_map<pair<Vertex, ColorSet>, pair<TrackInfo, VertexWeight> > table;
public:
  /// create an empty table
  LUT() {}

  /** check if a vertex is known to be part of a solution.

      @param v vertex to look up
      @param c color set to search v in
      @return true
      if @a v is known to be part of a motif instance of @a c. False
      otherwise, i.e. it is not part of such a motif instance or it is
      not known to be. */

  bool operator()(Vertex v, const ColorSet &c) const {
    return member(make_pair(v,c), table);
  }

  /** find out why a vertex is part of a certain solution.
      @param v vertex to look up
      @param c color set to search v in
      @return If @a v is part of a motif instance of @a c, return a
      ::TrackInfo value denoting why @a v is part of a motif instance
      of @a c. Fail otherwise*/
  TrackInfo info(Vertex v, const ColorSet &c) const {
    return table.find(make_pair(v,c))->second.first;
  }

  /** find out the weight of the most expensive instance of @a c, a
      vertex @a v occurs in.
      
      @param v vertex to look up
      @param c color set to search v in
      @return If @a v is part of a motif instance of @a c, return the
      weight of the heavies instance of @a c including @a v. */
  VertexWeight weight(Vertex v, const ColorSet &c) const {
    return table.find(make_pair(v,c))->second.second;
  }

  /** remember a solution.
      
      @post it is remembered, that @a v is part of a motif instance of
      @a c when @a c is constructed as denoted by the ::TrackInfo @a b. */

  void write(Vertex v, const ColorSet &c, const TrackInfo& b, VertexWeight w = 0) {
    table.erase(make_pair(v, c));
    table.insert(make_pair(make_pair(v, c), make_pair(b, w)));
  }

  /** clear the lookup table, remove all solutions */
  void clear() { table.clear(); }
};

/// type for saving solution vertices
typedef unordered_set<VertexID> Solution;

/// collects solution from backtracking information
class Tracker : public static_visitor<void> {
private:
  Vertex v;
  const Graph& G;
  const LUT &table;
  Solution& result;
public:
  /** constructor.
      @param _v vertex to start backtracking from
      @param _G graph to search solution in
      @param _M color set of which @a _v is known to be part of a motif instance of.
      @param _t table containing backtracking information
      @param _result pointer to a ::Solution object to push solution vertices onto */
  Tracker(Vertex _v, const Graph& _G,
	  const LUT& _t, Solution& _result)
    : v(_v), G(_G), table(_t), result(_result) {}

  /// collect solution vertices that make @a v a part of a motif instance of @a M
  void backtrack(Vertex v, const ColorSet &M);

  /// end backtracking, just add the current vertex to the result.
  void operator() (Leaf&);

  /// add the current vertex to the result and continue backtracking
  /// at its neighbour denoted by @a t and the current color set
  /// without the color of the current vertex.
  void operator() (Add& t);
};

/** get a solution from backtracking
    @param v vertex to start backtracking from
    @param G graph to search solution in
    @param M color set of which @a _v is known to be part of a motif instance of.
    @param t table containing backtracking information
    @return a ::Solution containing the vertices that are a graph motif instance of @a M. */

Solution backtrack(Vertex v, const Graph& G, const ColorSet& M, const LUT& t);

#endif

