/*   Gram -- A graph motif solver.
 *
 *   (C) 2008-2016 René van Bevern <rvb@nsu.ru>
 *
 *   This file is part of Gram.
 *
 *   Gram is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Gram is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Gram.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include <exception>
#include <cstring>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/graph/random.hpp>
#include <boost/variant.hpp>
#include <boost/math/special_functions.hpp>
#include <boost/graph/erdos_renyi_generator.hpp>
#include <boost/graph/connected_components.hpp>
#include "GraphUtils.hpp"

using namespace std;

typedef mt19937 generator_type;
typedef uniform_int<int> distribution;
typedef variate_generator<generator_type&, distribution> generator;

generator_type _RNG(time(0));
VertexIterator v0, vn;


dynamic_properties graph_properties(Graph& G) {
  dynamic_properties dp;
  VertexNames name = get(vertex_name, G);
  VertexColorLists colorlist = get(vertex_rank, G);
  EdgeWeights eweight = get(edge_weight, G);
	
  dp.property("node_id", name);
  dp.property("weight", eweight);
  dp.property("colorlist", colorlist);
  return dp;
}

Graph readGraphviz(istream& src) {
  Graph graph(0); 
    
  dynamic_properties dp = graph_properties(graph);
  read_graphviz(src, graph, dp, "node_id");

  VertexIndices index = get(vertex_index, graph);
  VertexIterator vi, vend;
  VertexIndex cnt = 0;

  for(tie(vi,vend) = vertices(graph); vi != vend; ++vi)
    put(index, *vi, cnt++);
  
  return graph;
}

Graph readGraphvizFromFile(const char* filename) {
  if(!strcmp(filename, "-"))
    return readGraphviz(cin);
  else {
    ifstream file(filename, ifstream::in);

    return readGraphviz(file);
  }
}

Graph& remove_edges(Graph &G, const EdgeFilter& f) {
  EdgeIterator e0, en, next;
  for(tie(e0, en) = edges(G), next = e0; e0 != en; e0 = next) {
    ++next;
    if(f(G, *e0)) remove_edge(*e0, G);
  }
  return G;
}

Graph& remove_vertices(Graph &G, const VertexFilter& f) {
  VertexIterator v0, vn, vi; tie(v0, vn) = vertices(G);
//  cout << "removing ";
  for (vi = v0; v0 != vn; v0 = vi) { ++vi;
    if(f(G, *v0)) {
//      cout << *v0 << " (" << get(vertex_name, G, *v0) << ") ";
      clear_vertex(*v0, G);
      remove_vertex(*v0, G);
    }
  }
//  cout << endl;
  return G;
}

vector<vector<Vertex> > colorset_intersection_graph(Graph &G) {
  VertexIterator v0, vn; tie(v0,vn) = vertices(G);
  VertexColorLists clist(get(vertex_rank, G));
  typedef adjacency_list<vecS, vecS, undirectedS> TmpGraph;
  typedef graph_traits<TmpGraph>::vertex_descriptor TmpVertex;

  TmpGraph res;
  unordered_map<Vertex, TmpVertex> iso;

  for(VertexIterator i = v0; i != vn; ++i)
    iso.insert(make_pair(*i, add_vertex(res)));

  
  for(VertexIterator i = v0; i != vn; ++i) 
    for(VertexIterator j = i; j != vn; ++j)
      if(*i != *j and !(ColorSet(clist[*i]) & ColorSet(clist[*j])).empty())
	add_edge(iso[*i], iso[*j], res);

  std::vector<TmpVertex> component(num_vertices(res));
  int num = connected_components(res, &component[0]);
  vector<vector<Vertex> > Ls(num);
  for(VertexIterator i = v0; i != vn; ++i)
    Ls[component[iso[*i]]].push_back(*i);

  return Ls;
}

MultiColorSet motifFromGraph(Graph &G) {
  VertexIterator v0, vn;
  VertexColorLists colorlist = get(vertex_rank, G);
  unordered_set<Color> collector;
  for (tie(v0, vn) = vertices(G); v0 != vn; ++v0) {
    ColorSet C(colorlist[*v0]);
    int s = C.size();
    int count = 0;
    ColorSet::key_type j = 0;
    while(count < s) {
      if(member(j, C)) { collector.insert(j); ++count; }
      ++j;
    }
  }
  return MultiColorSet(collector.begin(), collector.end());
}
