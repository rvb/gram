/*   Gram -- A graph motif solver.
 *
 *   (C) 2008-2016 René van Bevern <rvb@nsu.ru>
 *
 *   This file is part of Gram.
 *
 *   Gram is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Gram is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Gram.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GraphMotifInstance.hpp"
#include "GraphUtils.hpp"
#include "SubsetIterator.hpp"
#include <boost/unordered_set.hpp>
#include <boost/variant.hpp>
#include <boost/math/special_functions.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <numeric>
#include <algorithm>
#include <cstdio>

using namespace std;

inline ColorfulGraphMotifInstance::
ColorfulGraphMotifInstance(Graph& _G, ColorSet _M, int _k, ColorSet _L, bool _exact)
  : G(_G), M(_M), L(_L), k(_k), exact(_exact), coloring(get(vertex_color, G)),
    lists(get(vertex_rank, G)) {
//  cout << "------------------------------------------------------" << endl;
  VertexIterator v0, vn, v;
  remove_edges(G, unicolored_edge);
  remove_vertices(G, isolated);
  tie(v0, vn) = vertices(G);
  RankedSubsetIterator<ColorSet> end, c(M, 2);
  for(; c != end; ++c)
    for(v = v0; v != vn; ++v)
      if(member(coloring[*v], *c) && !(*c & ColorSet(lists[*v])).empty())
	table.write(*v, *c, Leaf());
}

inline bool ColorfulGraphMotifInstance::growMotifs(Vertex v, const ColorSet& C){
  ColorSet clist(get(vertex_rank,G,v));
  Graph::out_edge_iterator e0, en;
  VertexWeight w_opt = -1;
  Vertex v_opt;
  ColorSet F_opt;
  Edge e_opt;
  SubsetIterator<ColorSet> C_(C), end;
  for(++C_; C_ != end; ++C_)
    if(member(coloring[v], *C_)
       && !((*C_)&clist).empty()
       && (*C_-L).size() == (*C_&L).size()) {
	ColorSet F=*C_, coF=C-F;
	for(tie(e0, en) = out_edges(v, G); e0 != en; ++e0) {
	  Vertex a0 = target(*e0, G);
	  if(table(a0, coF) and table(v, F)) {
	    VertexWeight w = table.weight(a0, coF)
	    + table.weight(v, F)
	    + get(edge_weight, G, *e0);
	  if(w > w_opt) {
	    v_opt = a0;
	    F_opt = F;
	    w_opt = w;
	    e_opt = *e0;
	  }
	}
      }
    }
  if (w_opt >= 0) {
    table.write(v, C, Add(v, F_opt, v_opt, C-F_opt), w_opt);
    return true;
  } else return false;
}

inline tuple<VertexWeight, Solution> ColorfulGraphMotifInstance::solve() {
  RankedSubsetIterator<ColorSet> end;
  int relevantCount = 0, k_;
  VertexIterator v0, v, vn;
  
  for(k_ = 4; k_ <= k; k_+=2 ,relevantCount = 0) {
    RankedSubsetIterator<ColorSet> C(M, k_);
    unordered_set<Vertex> toKeep;
    for(tie(v0, vn) = vertices(G); C != end; ++C)
      for(v = v0; v != vn; ++v)
	if(member(coloring[*v], *C)
	   && !(*C & ColorSet(get(vertex_rank, G, *v))).empty()
	   && (*C-L).size() == (*C&L).size()
	   && growMotifs(*v, *C)
	   && toKeep.insert(*v).second)
	  ++relevantCount;
    if(exact && 2*relevantCount < k) return make_tuple(-1, Solution());
    if(2*relevantCount < k_) break;
    remove_vertices(G, is_not_in<unordered_set<Vertex> >(toKeep));
  }

  VertexWeight w_opt = -1;
  Vertex v_opt = 0;
  ColorSet C_opt;

  if(exact) {
    tie(v,vn) = vertices(G);
    return make_tuple(table.weight(*v, M), backtrack(*v, G, M, table));
  }
  
  for(tie(v, vn) = vertices(G); v != vn; ++v)
    for(RankedSubsetIterator<ColorSet> C(M, k_-2); C != end; ++C)
      if((*C-L).size() == (*C&L).size() && table(*v, *C)) {
	VertexWeight w = table.weight(*v, *C);
	if(w > w_opt) {
	  w_opt = w;
	  v_opt = *v;
	  C_opt = *C;
	}
      }
  if(w_opt >= 0) return make_tuple(w_opt, backtrack(v_opt, G, C_opt, table));
  else return make_tuple(-1, Solution());
}

int numcolors(vector<Vertex> L, Graph &G) {
  ColorSet allcolors;

  for(vector<Vertex>::iterator i = L.begin(); i != L.end(); ++i)
    allcolors = allcolors | ColorSet(get(vertex_rank, G, *i));

  return (allcolors.size() < L.size()) ? allcolors.size() : L.size();
}

int seed() {
  FILE *f = fopen("/dev/urandom", "r");
  int res;
  if(!f) res = time(0);
  else {
    res = getc(f);
    res = (res << 8) | getc(f);
  }
  if(f) fclose(f);
  cout << "random seed               : " << res << endl;
  return res;
}

GraphMotifInstance::GraphMotifInstance(const Graph& _G, const MultiColorSet& _M, float err, int randseed)
  : G(_G), M_(_M), k(_M.size()), tries(0), coloring(get(vertex_color, G)), errorProb(err), has_rng(randseed), successProb(0) {
  VertexIterator v0, vn;

  M = ColorSet(_M.begin(),_M.end());
  L = genLabels(_M);
  remove_vertices(G, isolated);
  Ls = colorset_intersection_graph(G);
  colorsPerL = vector<int>(Ls.size());
//  cout << "labels for indep. compon. : ";
  for(int i = 0; i < Ls.size(); ++i)
    colorsPerL[i] = numcolors(Ls[i], G);
//    cout << colorsPerL[i] << " ";
// }
//  cout << endl;
}

inline void GraphMotifInstance::recolor(const vector<bool>& how) {
  int curlabel = 0;
  for(int i = 0; i < Ls.size(); ++i) {
    if(how[i]) { // drawing random subset is better
      vector<Vertex> tocolor(Ls[i].begin(), Ls[i].end());
      for(int j = curlabel; j < curlabel + colorsPerL[i]; ++j) {
	int s = tocolor.size();
	int r = getRand(s);
	coloring[tocolor[r]] = L[j];
	tocolor.erase(tocolor.begin()+r);	
      }
      for(vector<Vertex>::iterator j = tocolor.begin(); j != tocolor.end(); ++j)
	coloring[*j] = L[curlabel + getRand(colorsPerL[i])];
    } else // colorcoding is better
      for(vector<Vertex>::iterator j = Ls[i].begin(); j != Ls[i].end(); ++j)
	coloring[*j] = L[curlabel + getRand(colorsPerL[i])];
    curlabel += colorsPerL[i];
  }
}

double trials(const vector<int> &colorsPerL, const vector<vector<Vertex> >& Ls, vector<bool> &how, float e) {
  double hit = 1;
  for(int i = 0; i < colorsPerL.size(); ++i) {
    double hita = math::factorial<double>(colorsPerL[i])
      / pow((double)colorsPerL[i],colorsPerL[i]);
    double hitb = 1 / math::binomial_coefficient<double>(Ls[i].size(), colorsPerL[i]);
    how[i] = hitb>hita;
    // if(hitb>hita) { cout << "hitb > hita" << endl; }
    //   if(hita>hitb) { cout << "hitb < hita" << endl; }
    //   if(hita==hitb) { cout << "hitb = hita" << endl; }				   
    hit *= hitb>hita?hitb:hita;
  }
  if(e<0) return hit;
  else return max(1UL, (long unsigned int)ceil(log(e)/log(1-hit)));
}

inline tuple<VertexWeight, Solution> GraphMotifInstance::solve() {
  vector<bool> how(Ls.size());
  if(M.empty()) return make_tuple(0,Solution());

  {
    int n = num_vertices(G);
    double hit = 1/(double)pow(2,n);
    double t = max(1UL, (long unsigned int)ceil(log(errorProb)/log(1-hit)));
    if(t <= (double)pow(4,k)) {
      tries = 1;
      return BruteForceInstance(G, M_, errorProb, RNG()).solve();
    }
  }
  
  double t = trials(colorsPerL, Ls, how, errorProb);
  successProb = t;
  tries = ceil(t);
  
  ColorSets rep;
  VertexWeight w_opt = -1;
  int w_size = 0;
  
  ColorSet LL(L.begin(), L.end());
  M = M | LL;
  k = M.size();

  Solution r_opt;
  for(int i = 0; i < t; ++i) {
    recolor(how);
    Solution result;
    VertexWeight w = -1;
      
    tie(w,result) = ColorfulGraphMotifInstance(G, M, k, LL, errorProb < 0).solve();

    if(result.size() > w_size || (result.size() >= w_size && w>w_opt)) {
      w_opt = w;
      w_size = result.size();
      r_opt = result;
    }
  }
//  cout << "solution weight           : " << w_opt << endl;
  return make_tuple(w_opt,r_opt);
}

inline double trials(int k, float e) {
  return math::factorial<double>(k)
      / pow((double)k,k);
}

inline double trials(int k, int k2, float e) {
  return 1/math::binomial_coefficient<double>(k, k2);
}

inline unordered_map<ColorSet, Color> MaxMotifInstance::colorcode(int k) {
  unordered_map<ColorSet, Color> res;
  for(MultiColorSet::iterator i = M.begin(); i != M.end(); ++i)
    res.insert(make_pair(ColorSet::singleton(*i), getRand(k)));
  return res;
}

inline MultiColorSet MaxMotifInstance::mapto_random_subset(int k) {
  MultiColorSet res;
  ColorPoolReps M_(M.begin(), M.end());
  for(int i = 1; i <= k; ++i) {
    int r(getRand(M_.size()));
    res.insert(M_[r]);
    M_.erase(M_.begin()+r);
  }
  return res;
}

inline Graph remap_colorlists(Graph G, const unordered_map<ColorSet, Color>& map) {
  VertexColorLists clist(get(vertex_rank, G));
  VertexIterator v0, vn; tie(v0,vn) = vertices(G);
  for(VertexIterator i = v0; i != vn; ++i) {
    ColorSet C(clist[*i]), C_;
    RankedSubsetIterator<ColorSet> j(C,1), end;
    for(; j != end; ++j)
      C_.insert(map.find(*j)->second);
    clist[*i] = C_.rep;
  }
  return G;
}

inline void constrain_colorlists(Graph &G, const MultiColorSet& M) {
  ColorSet C(M.begin(), M.end());
  VertexColorLists clist(get(vertex_rank, G));
  VertexIterator v0, vn; tie(v0,vn) = vertices(G);

  for(VertexIterator i = v0; i != vn; ++i)
    clist[*i] = (ColorSet(clist[*i]) & C).rep;
}

inline MultiColorSet remap_motif(MultiColorSet& M, const unordered_map<ColorSet, Color>& map) {
  unordered_set<Color> res;
  for(MultiColorSet::iterator i = M.begin(); i != M.end(); ++i)
    res.insert(map.find(ColorSet::singleton(*i))->second);
  return MultiColorSet(res.begin(), res.end());
}

MaxMotifInstance::MaxMotifInstance(const Graph &_G, const MultiColorSet &_M,
				   float err, int seed) : G(_G), M(_M), errorProb(err), has_rng(seed), tries(0) {
}

tuple<VertexWeight, Solution> MaxMotifInstance::solve() {
  Solution r_opt;
  int k = M.size(), w_size = 0, w_opt = 0;
  int n = num_vertices(G);
  tries = 0;
  for(int k_ = 2; k_ <= k; k_++) {
    int tr = 0;

    if(n < k_) break;
    Solution result;
    VertexWeight w;
    // try using brute force guessing
    if(k-k_+2 <= n) {
      double hit = trials(n, k-k_+2, errorProb);
      double t = max(1UL, (long unsigned int)ceil(log(errorProb)/log(1-hit)));
      if(t <= (double)pow(4,k)) {
	VertexWeight w;
	Solution result;
	tie(w, result) = BruteForceInstance(G, M, errorProb, RNG(), k-k_+2).solve();
	tr++;
	if(result.size() > w_size || (result.size() >= w_size && w>w_opt)) {
	  r_opt = result;
	  w_opt = w;
	  w_size = result.size();
	  k_ = k-k_+3;
	}
//	if(!result.empty()) return make_tuple(w, result);
      }
      if(k_>k) break;
    }
    
    // color coding
    double t1(trials(k_, errorProb));
    double t2(trials(k, k_, errorProb));
    double t=(t2>t1?t2:t1);
    double err = 1;
    while(err >= errorProb) {
      Graph G_;
      MultiColorSet M_;
      if(t2>t1) {
	M_ = mapto_random_subset(k_);
	G_ = G;
	remove_vertices(G_, colorlist_disjoint_from(ColorSet(M_.begin(), M_.end())));
	if(num_vertices(G_) < k_) {
	  err *= (1-t*t1);
	  continue;
	}
	constrain_colorlists(G_, M_);
      } else {
	unordered_map<ColorSet, Color> map(colorcode(k_));
	G_ = remap_colorlists(G, map);
	M_ = remap_motif(M, map);
	if(M_.size() < k_) {
	  err *= (1-t*t1);
	  continue;
	}
      }
      Solution result;
      VertexWeight w;
      GraphMotifInstance GMI(G_, M_, -1, RNG());
      tr++;
      tie(w,result) = GMI.solve();
      if(result.size() > w_size || (result.size() >= w_size && w>w_opt)) {
	w_opt = w;
	w_size = result.size();
	r_opt = result;
      }
      err *= (1-t*GMI.successProb);
    }
    tries += tr;
    if(r_opt.size() < k_) break;
  }
  return make_tuple(w_opt,r_opt);
}

BruteForceInstance::BruteForceInstance(const Graph &_G, const MultiColorSet &_M, float err, int seed, int _size) : G(_G), M(_M), errorProb(err), size(_size), has_rng(seed), tries(1) {
}

unordered_set<Vertex> BruteForceInstance::random_vertices(const Graph &G, int k) {
  VertexIterator v0, vn;
  tie(v0,vn) = vertices(G);
  vector<Vertex> verts(v0, vn);
  unordered_set<Vertex> res;
  for(int i = 1; i <= k; ++i) {
    int r(getRand(verts.size()));
    res.insert(verts[r]);
    verts.erase(verts.begin()+r);
  }
  return res;
}

int mst_weight(const Graph &_G, const unordered_set<Vertex>& vs) {
  unordered_set<VertexID> ids;
  for(unordered_set<Vertex>::iterator i = vs.begin(); i != vs.end(); ++i)
    ids.insert(get(vertex_index, _G, *i));
  Graph G(_G);
  remove_vertices(G, index_not_in<unordered_set<VertexID> >(ids));
  VertexIterator v0, vn;
  tie(v0, vn) = vertices(G);
  for(int i = 0; v0 != vn; ++v0) put(vertex_index, G, *v0, i++);
  
  unordered_map<Vertex, int> component;
  map<Vertex, int> colors;
  associative_property_map< unordered_map<Vertex, int> > component_map(component);
  int num = connected_components(G, component_map, color_map(make_assoc_property_map(colors)));
  if(num > 1) return -1; // no MST

  
  EdgeIterator e0, em;
  tie(e0, em) = edges(G);
  for(; e0 != em; ++e0) put(edge_weight, G, *e0, -get(edge_weight, G, *e0));
  list<Edge> mst;
  kruskal_minimum_spanning_tree(G, back_inserter(mst));// inserter(mst, mst.begin()));
  int weight = 0;
  for(list<Edge>::iterator i = mst.begin(); i != mst.end(); ++i) {
    weight += -get(edge_weight, G, *i);
  }
  return weight;
}

Graph vertex_color_incidence_graph(const Graph &G, const MultiColorSet &M, const unordered_set<Vertex> &vs) {
  Graph G_;
  int index = 0;
  unordered_map<ColorSet, Vertex> color2vertex;
  unordered_map<Vertex, Vertex> vertex2vertex;
  for(MultiColorSet::iterator i = M.begin(); i != M.end(); ++i) {
    Vertex v = add_vertex(G_);
    color2vertex[ColorSet::singleton(*i)] = v;
    put(vertex_index, G_, v, index++);
  }
  for(unordered_set<Vertex>::iterator i = vs.begin(); i != vs.end(); ++i) {
    Vertex v = add_vertex(G_);
    put(vertex_index, G_, v, index++);
    ColorSet C(get(vertex_rank, G, *i));
    RankedSubsetIterator<ColorSet> c(C, 1), end;
    for(; c != end; ++c)add_edge(v, color2vertex[*c], G_);
  }

  return G_;
}

int matching_size(const Graph &G) {
  unordered_map<Vertex, Vertex> mates;
  associative_property_map< unordered_map<Vertex, Vertex> > mate_map(mates);
  edmonds_maximum_cardinality_matching(G, mate_map);
  int size = 0;
  for(unordered_map<Vertex, Vertex>::iterator i = mates.begin(); i != mates.end(); ++i)
    if(i->first != graph_traits<Graph>::null_vertex()
       && i->second != graph_traits<Graph>::null_vertex()) size++;
  return size/2;
}

void incf(int* k) {
  *k++;
}

void decf(int* k) {
  *k--;
}

tuple<VertexWeight, Solution> BruteForceInstance::solve() {
  int k;
  int k_;

  if(size>0) {
    k = k_ = size;
  } else {
    k = 2;
    k_ = min(M.size(), num_vertices(G));
  }

  int w_opt = 0;
  Solution S_opt;
  for(; k_ >= k; --k_) {
    double hit = trials(num_vertices(G), k_, errorProb);
    int t = max(1UL, (long unsigned int)ceil(log(errorProb)/log(1-hit)));
    for(int t_ = 0; t_ < t; ++t_) {
      unordered_set<Vertex> vs = random_vertices(G, k_);
      int w = mst_weight(G, vs);
      if(w < 0) continue;
      if(k_ > S_opt.size() || (k_ >= S_opt.size() && w > w_opt)) {
	Graph G_(vertex_color_incidence_graph(G, M, vs));
	if(matching_size(G_) >= k_){
	  w_opt = w;
	  S_opt.clear();
	  for(unordered_set<Vertex>::iterator i = vs.begin(); i != vs.end(); ++i)
	    S_opt.insert(get(vertex_name, G, *i));
	  k = k_;
	}
      }
    }
  }
  return make_tuple(w_opt, S_opt);
}

