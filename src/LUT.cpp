/*   Gram -- A graph motif solver.
 *
 *   (C) 2008-2016 René van Bevern <rvb@nsu.ru>
 *
 *   This file is part of Gram.
 *
 *   Gram is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Gram is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Gram.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LUT.hpp"

int depth;

string depthspace(int i) {
  string res;
  for(; i>=0; --i){
    res+=" ";
  }
  return res;
}

void Tracker::backtrack(Vertex v, const ColorSet &M) {
    TrackInfo i(table.info(v, M));
    Tracker run(v, G, table, result);
    apply_visitor(run, i);
}

Solution backtrack(Vertex v, const Graph& G, const ColorSet& M, const LUT& table) {
    Solution result;
    Tracker run(v, G, table, result);
    run.backtrack(v, M);
    return result;
}

inline void Tracker::operator() (Leaf&) {
  result.insert(get(vertex_name, G, v));
}

inline void Tracker::operator() (Add& t) {
    result.insert(get(vertex_name, G, v));
    backtrack(t.v, t.vc);
    backtrack(t.u, t.uc);
}

