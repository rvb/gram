/*   Gram -- A graph motif solver.
 *
 *   (C) 2008-2016 René van Bevern <rvb@nsu.ru>
 *
 *   This file is part of Gram.
 *
 *   Gram is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Gram is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Gram.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/function.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/graph/connected_components.hpp>
#include "GraphUtils.hpp"
#include "LUT.hpp"

typedef unordered_map<Color, ColorSet> ColorSets;

/** find colorful graph motives in graphs */
  class ColorfulGraphMotifInstance {
protected:
  typedef vector<ColorSet> Reps;
  
  /** colorful motif being sought */
  ColorSet M;
  ColorSet L;

  bool exact;
    
  /** size of colorful motif being sought */
  int k;

  /** current mapping of vertices to vertex colors */
  const VertexColors& coloring;
  const VertexColorLists& lists;

  /** lookup table for dynamic programming. for each vertex @a v and
      each colorset @a C stores, if v is part of an occurence of @a C
      in #G. */
  LUT table;

  /** graph that is searched for the motif */
  Graph G;

  /** check if a vertex can be added to existing solutions.

      @pre #table has to store the correct
      information for all neighbors of @a v and all subsets of @a C.

      @post if @a v is part of @a C is stored in #table.

      @return it is returned if @a v is part of @a C. */
      
  bool growMotifs(Vertex v, const ColorSet& C);

  /** check if two two existing solutions can be joined.
      @pre #table has to store the correct
      information for all neighbors of @a v and all subsets of @a C.
      
      @post if @a v is part of @a C is stored in #table.
      
      @return it is returned if @a v is part of @a C. */
  bool joinMotifs(Vertex v, const ColorSet& C, int thres);
public:

  /** find graph motif @return a solution of size #k if an instance of
      #M can be found in #G, otherwise an empty solution.  @post if
      DESTRUCTIVE is defined at time of compilation,
      all vertices that are not relevant for a solution are deleted in
      each iteration step.  A vertex is relevant for a solution of
      size @a k if it part of a solution of size @a (k-1) */
  tuple<VertexWeight, Solution> solve();

  /** Construct colorful graph motif instance. The following
   * preprocessing steps are done here:
      - initialization of the backtracking table
      - removal of isolated vertices if @a k is greater than 1
      - removal of unicolored edges

      @param G graph to search motif in
      @param M motif an instance is to be found of in @a G
      @param k size of motif instance to be found
  */
      
  ColorfulGraphMotifInstance(Graph& G, ColorSet M, int k, ColorSet L, bool exact = false);
};

int seed();

class has_rng {
protected:
  typedef mt19937 generator_type;
  typedef uniform_int<int> distribution;
  typedef variate_generator<generator_type&, distribution> generator;
  
  generator_type RNG;

  has_rng() : RNG(seed()) {}
  has_rng(uint32_t se) { RNG.seed(se); }
  
  int getRand(int max) {
    return generator(RNG, distribution(0, max - 1))();
  }

public:
  /** reseed the internal random generator */
  void reseed() { RNG.seed(RNG); }  
};

/** find general graph motives in graphs */
class GraphMotifInstance : protected has_rng {
protected:
  /** colorful motif being sought */
  ColorSet M;
  MultiColorSet M_;

  /** size of colorful motif being sought */
  int k;

  /** current mapping of vertices to vertex colors */
  VertexColors coloring;

  /** graph that is searched for the motif */
  Graph G;

  ColorPoolReps L;
  vector<vector<Vertex> > Ls;
  vector<int> colorsPerL;
  
  float errorProb;

  /** recolor the graph.  @post the vertices in #toRecolor are
      destructively given new colors from #pool. */
  void recolor(const vector<bool>& how);
public:
  /** construct graph motif instance. The following preprocessing is done in this step:
      - vertices with wrong color are deleted
      - isolated vertices are removed if M has more than one element
      @param G graph to search motif in
      @param M motif an instance is to be found of in @a G
      @param err the probability with which the algorithm may not find a
      solutuion. This value defaults to 0.1.
  */
  GraphMotifInstance(const Graph& G, const MultiColorSet& M, float err = 0.1, int randseed = seed());

  double successProb;
  int tries;

  /** find graph motif @return a solution of size #k if an instance of
      #M can be found in #G, otherwise #recolor #G and try again, until
      a certain number of ::trials is exceeded. */
  tuple<VertexWeight, Solution> solve();
};

class MaxMotifInstance : protected has_rng {
protected:
  MultiColorSet M;
  Graph G;
  float errorProb;
  
  unordered_map<ColorSet, Color> colorcode(int k);
  MultiColorSet mapto_random_subset(int k);
public:
  MaxMotifInstance(const Graph& G, const MultiColorSet &M, float err, int seed = seed());
  tuple<VertexWeight, Solution> solve();
  int tries;
};

class BruteForceInstance : protected has_rng {
protected:
  MultiColorSet M;
  Graph G;
  float errorProb;
  int size;
  bool topdown;
  unordered_set<Vertex> random_vertices(const Graph&, int);
public:
  BruteForceInstance(const Graph &G, const MultiColorSet &M, float err, int seed = seed(), int size = -1);
  tuple<VertexWeight, Solution> solve();
  int tries;
};

template <class T>
tuple<VertexWeight, Solution> solve_components_independently(const Graph &G, MultiColorSet M, float err, int s = seed()) {
  unordered_map<Vertex, int> component;
  map<Vertex, int> colors;
  associative_property_map< unordered_map<Vertex, int> > component_map(component);
  int num = connected_components(G, component_map, color_map(make_assoc_property_map(colors)));
  if(num < 1) return make_tuple(0, Solution());
  cout << "number of connected comp's: " << num       << endl;
  
  vector<int> components(num, 0);
  unordered_map<int, int> components_;
  VertexIterator v0, vn;
  tie(v0,vn) = vertices(G);
  for(; v0 != vn; ++v0) {
    components_[get(vertex_index, G, *v0)] = component[*v0];
    components[component[*v0]]++;
  }

  VertexWeight w_opt = -1;
  Solution S_opt;
  int tries = 0;
  int C_opt = 0;
  for(int i = 0; i < num; ++i) {
    Graph G_(G);
    remove_vertices(G_, index_not_mapsto<unordered_map<int, int> >(components_, i));
    VertexWeight w;
    Solution S;
    M = motifFromGraph(G_);
    T GMI(G_,M,err,s);
    tie(w, S) = GMI.solve();
    tries = max(tries, GMI.tries);
    if(S.size() > S_opt.size() || (S.size() >= S_opt.size() && w>w_opt)) {
      S_opt = S;
      w_opt = w;
      C_opt = i;
    }
  }
  cout << "recoloring tries used     : " << tries << endl;
  cout << "component size for opt.   : " << components[C_opt] << endl;
  return make_tuple(w_opt, S_opt);
}

