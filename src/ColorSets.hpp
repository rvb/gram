/*   Gram -- A graph motif solver.
 *
 *   (C) 2008-2016 René van Bevern <rvb@nsu.ru>
 *
 *   This file is part of Gram.
 *
 *   Gram is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Gram is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Gram.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _COLORSETS_H
#define _COLORSETS_H

#include <set>
#include <vector>
#include <exception>
#include <iostream>
#include <iterator>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <boost/tuple/tuple.hpp>
#include "EfficientSet.hpp"

using namespace boost;
using namespace std;

/// Color.
typedef int Color;

/** generic function to check membership of elements in collections.
    @tparam T collection type
    @param e element to search for
    @param M collection to search in
    @return true or false, dependant of \f$e\in M\f$. */
template <typename T>
inline bool member(typename T::key_type e, const T &M) {
     return (M.find(e) != M.end());
}

/// Color set type. It should allow fast iteration over its subsets.
typedef EfficientSet<Color> ColorSet;

/// color multisets
typedef multiset<Color> MultiColorSet;

/// collection type that holds the replacement colors for a ::ColorPool,
/// it must support fast random access.
typedef vector<Color> ColorPoolReps;

/// type to associate with each color @a c a set of colors that
/// may be used to replace $a c in recoloring the graph to achieve a
/// colorful recoloring.
typedef unordered_map<Color, ColorPoolReps > ColorPool;

ColorPoolReps genLabels(const MultiColorSet &M);
#endif
