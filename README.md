Gram --- Find connected motifs in biological networks
=====================================================

Gram is a tool to find connected motifs in biological networks, such as
protein-protein interaction networks.  More specifically, it solves the
following NP-hard problem:

**Input:** A graph _G_, each vertex of which has a set of admissible colors, and a multiset of colors _M_.   
**Output:** A maximum-cardinality subset _S_ of vertices such that _G[S]_ is connected and such that each of _S_ can be colored using one of its admissible colors such that the multiset of colors used by vertices in _S_ is a submultiset of _M_.

Gram solves the problem using a _fixed-parameter linear time_ algorithm, whose running time depends linearly on the size of _G_ and exponentially only on the size of the multiset _M_ of colors.  The algorithm is described in

- [N. Betzler, R. van Bevern, M. R. Fellows, C. Komusiewicz, and
      R. Niedermeier. Parameterized algorithmics for finding connected
      motifs in biological networks. _IEEE/ACM Transactions on
      Computational Biology and Bioinformatics_, 8(5):1296–1308,
      2011.](http://dx.doi.org/10.1109/TCBB.2011.19)

Gram has been tested on:

- Debian GNU/Linux (x86_64) 5.0 with GCC 4.3.2
- Debian GNU/Linux (x86_64) 8.6 with GCC 4.9.2
- Arch Linux (x86_64) with GCC 4.5
- Mac OS X 10.5 with GCC 4.2

You can download Gram by downloading a [Zip
archive](https://gitlab.com/rvb/gram/repository/archive.zip?ref=master)
or [tar.gz
archive](https://gitlab.com/rvb/gram/repository/archive.tar.gz?ref=master)
or by cloning this git repository

```
git clone https://gitlab.com/rvb/gram.git
```

## Requirements / Installation

Gram needs the [Boost Library](http://www.boost.org) in version 1.36 or
higher, including the Boost Graph Library, and the
[CMake](https://cmake.org/) build system.  If all requirements are met,
compiling Gram should work as follows:

In the directory that this README
file is in, execute:

```
cmake .
make
```
The result should be a `gram` executable.

## Usage

Call Gram like
```
gram -e 0.1 file.dot
```
Herein, `-e 0.1` denotes the probability of error and is an optional
parameter. Per default, 0.1 is used as maximum error probability. The
input graph is given as `file.dot` here. If replaced by `-`, a graph
is read from standard input. The file format is described below.

Gram then searches for an occurrence of a motif _M_ that contains all
colors that are assigned to some vertex in the given input graph. It
outputs a maximum set of connected vertices that are an occurrence of a
submotif of _M_. If there are multiple such maximum connected vertex
sets, the one that has the spanning tree of maximum weight is output.

Because of data reduction rules, Gram does in general not output motif
occurrences containing only a single vertex. Observe that every
query allows for a motif occurrence of only one vertex. Thus, these
are "trivial" solutions. If only trivial solutions exist, Gram may
output "no nontrivial motif occurrence found".

In this case, the maximum set of connected vertices that are an
occurrence of a submotif of _M_ has size 1 and every vertex of the input
graph is an occurrence.

## Random Number Generator Initialization

If you use Gram on a system that provides `/dev/urandom`, a value from
this device is used as seed for the randon number generator. Otherwise,
the system time in seconds is used as seed. Thus, beware of rapidly
solving the same graph motif instances in a short time interval, because
the same seed for the random number generator is being used every time.

## File Format and Limitations on Input

Input graphs should be given in graphviz format. For example, see the
file `examplegraph.dot` that is distributed with Gram. Each edge must
have a `weight` attribute that gives the weight of an edge.

Each vertex must have a `colorlist` attribute that gives a list of
colors for this vertex. This list is binary coded as a 64bit
integer. That is, if a vertex has the color list 0, 1, 3, 5, 7, this is
encoded as 2^0 + 2^1 + 2^3 + 2^5 + 2^7 and the according entry for
vertex number 16 is:
```
16 [colorlist="171"]
```
This encoding of color sets is for speed reasons, as it easily allows
the enumeration of subsets. However, it has the limitation that the
largest color in use can have the number 63. So a graph motif instance
with more than 32 colors cannot be solved because another colors are
used for color coding.

Vertices must have integer names.

## Example run / Explaining output

Consider the following execution step:

Input: gram examplegraph.dot
Output with explaining comments:
```
colorful motif size       : 6
```
This line shows the size of the motif of which an occurrence is
sought.
```
maximum error probability : 0.1
random seed               : 35715
```
Shows the seed that has been used to initialize the random number
generator. It can be used to reproduce a specific run of Gram by
initializing the random number generator to this value.
```
recoloring tries used     : 1
```
Number of recoloring trials for color coding. The lower, the
better.
```
solution size             : 6
```
The size of the found motif occurrence. Here, we have a perfect match.
```
solution vertices         : 1335 2420 2740 362 525 1428 
```
The vertices that constitue the solution set, if the solution size is
at least two. Otherwise, no vertices might be output, but every vertex
is a solution vertex.

-----
René van Bevern <rvb@nsu.ru>
